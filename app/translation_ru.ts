<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU" sourcelanguage="en">
<context>
    <name>About</name>
    <message>
        <location filename="about.ui" line="35"/>
        <location filename="about.ui" line="305"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="about.ui" line="150"/>
        <location filename="about.ui" line="263"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="about.ui" line="169"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:120%;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Cine Encoder 2021 ver. 3.4&lt;br /&gt;Program for encoding HDR and SDR video.&lt;br /&gt;&lt;br /&gt;This software is free for personal and commercial use. It is distributed in the hope that it is useful but without any warranty. See the GNU General Public Licence v3 for more information.&lt;br /&gt;&lt;br /&gt;&lt;/span&gt;&lt;a href=&quot;https://github.com/CineEncoder/cine-encoder&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#0000ff;&quot;&gt;https://github.com/CineEncoder/cine-encoder&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/span&gt;&lt;a href=&quot;https://github.com/CineEncoder/cine-encoder/blob/master/LICENSE&quot;&gt;&lt;span style=&quot; font-size:8pt; text-decoration: underline; color:#0000ff;&quot;&gt;License: GNU General Public License Version 3&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;br /&gt;Copyright (C) 2020-2021 Oleg Kozhukharenko&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:120%;&quot;&gt;Cine Encoder 2021 ver. 3.4&lt;br /&gt;Program for encoding HDR and SDR video.&lt;br /&gt;&lt;br /&gt;This software is free for personal and commercial use. It is distributed in the hope that it is useful but without any warranty. See the GNU General Public Licence v3 for more information.&lt;br /&gt;&lt;br /&gt;&lt;a href=&quot;https://github.com/CineEncoder/cine-encoder&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;https://github.com/CineEncoder/cine-encoder&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;a href=&quot;https://github.com/CineEncoder/cine-encoder/blob/master/LICENSE&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#007af4;&quot;&gt;License: GNU General Public License Version 3&lt;/span&gt;&lt;/a&gt;&lt;br /&gt;&lt;br /&gt;Copyright (C) 2020-2021 Oleg Kozhukharenko&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation></translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Cantarell&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translatorcomment>HTML</translatorcomment>
        <translation type="vanished">&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height: 120%;&quot;&gt;

Cine Encoder 2021 ver. 3.4
&lt;br /&gt;

Программа для конвертации видео HDR и SDR.
&lt;br /&gt;&lt;br /&gt;

Это программное обеспечение является бесплатным для личного и коммерческого использования. Он распространяется в надежде, что он полезен, но без каких-либо гарантий. Дополнительную информацию смотрите в разделе GNU General Public License v3.
&lt;br /&gt;&lt;br /&gt;

&lt;a href=&quot;https://github.com/CineEncoder/cine-encoder&quot;&gt;https://github.com/CineEncoder/cine-encoder&lt;/a&gt;&lt;/p&gt;
&lt;br /&gt;

&lt;a href=&quot;https://github.com/CineEncoder/cine-encoder/blob/master/LICENSE&quot;&gt;License: GNU General Public License Version 3&lt;/a&gt;&lt;/p&gt;
&lt;br /&gt;&lt;br /&gt;

Copyright (C) 2020-2021 Oleg Kozhukharenko

&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</translation>
    </message>
    <message>
        <location filename="about.cpp" line="45"/>
        <source>Program for encoding HDR and SDR video.</source>
        <translation>Программа для кодирования HDR и SDR видео.</translation>
    </message>
    <message>
        <location filename="about.cpp" line="45"/>
        <source>This software is free for personal and commercial use. It is distributed in the hope that it is useful but without any warranty. See the GNU General Public Licence v3 for more information.</source>
        <translation>Это программное обеспечение является бесплатным для личного и коммерческого использования. Оно распространяется в надежде, что будет полезно, но без каких-либо гарантий. Дополнительную информацию смотрите в разделе GNU General Public License v3.</translation>
    </message>
    <message>
        <location filename="about.cpp" line="48"/>
        <source>License: GNU General Public License Version 3</source>
        <translation>Лицензия: GNU General Public License Version 3</translation>
    </message>
    <message>
        <location filename="about.cpp" line="49"/>
        <source>Copyright</source>
        <translation>Copyright</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="dialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="dialog.ui" line="210"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="dialog.ui" line="226"/>
        <source>Cine Encoder</source>
        <translation>Cine Encoder</translation>
    </message>
    <message>
        <location filename="dialog.ui" line="288"/>
        <source>Ok</source>
        <translation>Ок</translation>
    </message>
    <message>
        <location filename="dialog.ui" line="334"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="dialog.ui" line="369"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>Donate</name>
    <message>
        <location filename="donate.ui" line="32"/>
        <location filename="donate.ui" line="235"/>
        <source>Donate</source>
        <translation>Донат</translation>
    </message>
    <message>
        <location filename="donate.ui" line="206"/>
        <location filename="donate.ui" line="365"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="donate.ui" line="297"/>
        <source>PayPal</source>
        <translation>PayPal</translation>
    </message>
    <message>
        <location filename="donate.ui" line="331"/>
        <source>Bitcoin</source>
        <translation>Bitcoin</translation>
    </message>
    <message>
        <location filename="donate.ui" line="384"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Cantarell&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height: 120%;&quot;&gt;

Cine Encoder 2021 ver. 3.4
&lt;br /&gt;

Программа для конвертации видео HDR и SDR.
&lt;br /&gt;&lt;br /&gt;

Это программное обеспечение является бесплатным для личного и коммерческого использования. Он распространяется в надежде, что он полезен, но без каких-либо гарантий. Дополнительную информацию смотрите в разделе GNU General Public License v3.
&lt;br /&gt;&lt;br /&gt;

&lt;a href=&quot;https://github.com/CineEncoder/cine-encoder&quot;&gt;https://github.com/CineEncoder/cine-encoder&lt;/a&gt;&lt;/p&gt;
&lt;br /&gt;

&lt;a href=&quot;https://github.com/CineEncoder/cine-encoder/blob/master/LICENSE&quot;&gt;License: GNU General Public License Version 3&lt;/a&gt;&lt;/p&gt;
&lt;br /&gt;&lt;br /&gt;

Copyright (C) 2020-2021 Oleg Kozhukharenko

&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;
</translation>
    </message>
    <message>
        <location filename="donate.cpp" line="46"/>
        <source>This software is free for personal and commercial use. It is distributed in the hope that it is useful but without any warranty. See the GNU General Public Licence v3 for more information.</source>
        <translation>Это программное обеспечение является бесплатным для личного и коммерческого использования. Оно распространяется в надежде, что будет полезно, но без каких-либо гарантий. Дополнительную информацию смотрите в разделе GNU General Public License v3.</translation>
    </message>
    <message>
        <location filename="donate.cpp" line="49"/>
        <source>If you find this application useful, consider making a donation to support the development.</source>
        <translation>Если вы считаете это приложение полезным, подумайте о том, чтобы сделать пожертвование в поддержку разработки.</translation>
    </message>
</context>
<context>
    <name>OpeningFiles</name>
    <message>
        <location filename="openingfiles.ui" line="26"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="openingfiles.ui" line="152"/>
        <source>Opening Files</source>
        <translation>Открытие файлов</translation>
    </message>
    <message>
        <location filename="openingfiles.ui" line="246"/>
        <source>Filename:</source>
        <translation>Имя файла:</translation>
    </message>
</context>
<context>
    <name>Preset</name>
    <message>
        <location filename="preset.ui" line="35"/>
        <location filename="preset.ui" line="670"/>
        <location filename="preset.ui" line="3021"/>
        <source>Preset</source>
        <translation>Пресет</translation>
    </message>
    <message>
        <location filename="preset.ui" line="205"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="preset.ui" line="234"/>
        <source>Edit preset</source>
        <translation>Редактирование пресета</translation>
    </message>
    <message>
        <location filename="preset.ui" line="345"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="preset.ui" line="379"/>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="preset.ui" line="465"/>
        <location filename="preset.ui" line="560"/>
        <source>Transform</source>
        <translation>Преобразование</translation>
    </message>
    <message>
        <location filename="preset.ui" line="484"/>
        <location filename="preset.ui" line="1793"/>
        <source>Video</source>
        <translation>Видео</translation>
    </message>
    <message>
        <location filename="preset.ui" line="503"/>
        <location filename="preset.ui" line="3361"/>
        <location filename="preset.ui" line="3442"/>
        <source>Audio</source>
        <translation>Аудио</translation>
    </message>
    <message>
        <location filename="preset.ui" line="522"/>
        <source>Metadata</source>
        <translation>Метаданные</translation>
    </message>
    <message>
        <location filename="preset.ui" line="719"/>
        <source>          Result </source>
        <translation>                Итог</translation>
    </message>
    <message>
        <location filename="preset.ui" line="778"/>
        <source>Frame Rate</source>
        <translation>Частота кадров</translation>
    </message>
    <message>
        <location filename="preset.ui" line="817"/>
        <source>Simple</source>
        <translation>Simple</translation>
    </message>
    <message>
        <location filename="preset.ui" line="822"/>
        <source>Interpolated</source>
        <translation>Interpolated</translation>
    </message>
    <message>
        <location filename="preset.ui" line="827"/>
        <source>MCI</source>
        <translation>MCI</translation>
    </message>
    <message>
        <location filename="preset.ui" line="832"/>
        <source>Blend</source>
        <translation>Blend</translation>
    </message>
    <message>
        <location filename="preset.ui" line="894"/>
        <location filename="preset.ui" line="1083"/>
        <location filename="preset.ui" line="1311"/>
        <location filename="preset.ui" line="1534"/>
        <location filename="preset.ui" line="1757"/>
        <location filename="preset.ui" line="2625"/>
        <location filename="preset.ui" line="3502"/>
        <location filename="preset.ui" line="3684"/>
        <location filename="preset.ui" line="3847"/>
        <location filename="preset.ui" line="4007"/>
        <location filename="preset.ui" line="4054"/>
        <location filename="preset.ui" line="4115"/>
        <location filename="preset.ui" line="4287"/>
        <location filename="preset.ui" line="4378"/>
        <location filename="preset.ui" line="4447"/>
        <location filename="preset.ui" line="4469"/>
        <location filename="preset.ui" line="4629"/>
        <location filename="preset.ui" line="4771"/>
        <location filename="preset.ui" line="4894"/>
        <location filename="preset.ui" line="4913"/>
        <location filename="preset.ui" line="5015"/>
        <location filename="preset.cpp" line="585"/>
        <location filename="preset.cpp" line="587"/>
        <location filename="preset.cpp" line="588"/>
        <location filename="preset.cpp" line="596"/>
        <location filename="preset.cpp" line="597"/>
        <location filename="preset.cpp" line="705"/>
        <location filename="preset.cpp" line="706"/>
        <location filename="preset.cpp" line="707"/>
        <location filename="preset.cpp" line="708"/>
        <location filename="preset.cpp" line="709"/>
        <location filename="preset.cpp" line="710"/>
        <location filename="preset.cpp" line="711"/>
        <location filename="preset.cpp" line="712"/>
        <location filename="preset.cpp" line="713"/>
        <location filename="preset.cpp" line="714"/>
        <location filename="preset.cpp" line="727"/>
        <location filename="preset.cpp" line="767"/>
        <location filename="preset.cpp" line="836"/>
        <location filename="preset.cpp" line="892"/>
        <location filename="preset.cpp" line="956"/>
        <location filename="preset.cpp" line="957"/>
        <location filename="preset.cpp" line="959"/>
        <location filename="preset.cpp" line="962"/>
        <location filename="preset.cpp" line="965"/>
        <location filename="preset.cpp" line="1100"/>
        <location filename="preset.cpp" line="1115"/>
        <location filename="preset.cpp" line="1131"/>
        <location filename="preset.cpp" line="1153"/>
        <location filename="preset.cpp" line="1175"/>
        <location filename="preset.cpp" line="1192"/>
        <location filename="preset.cpp" line="1208"/>
        <location filename="preset.cpp" line="1225"/>
        <location filename="preset.cpp" line="1239"/>
        <location filename="preset.cpp" line="1252"/>
        <location filename="preset.cpp" line="1453"/>
        <location filename="preset.cpp" line="1478"/>
        <location filename="preset.cpp" line="1628"/>
        <location filename="preset.cpp" line="1629"/>
        <location filename="preset.cpp" line="1696"/>
        <location filename="preset.cpp" line="1697"/>
        <location filename="preset.cpp" line="1698"/>
        <source>Source</source>
        <translation>Исходный</translation>
    </message>
    <message>
        <location filename="preset.ui" line="899"/>
        <source>120</source>
        <translation>120</translation>
    </message>
    <message>
        <location filename="preset.ui" line="904"/>
        <source>60</source>
        <translation>60</translation>
    </message>
    <message>
        <location filename="preset.ui" line="909"/>
        <source>59.940</source>
        <translation>59.940</translation>
    </message>
    <message>
        <location filename="preset.ui" line="914"/>
        <location filename="preset.ui" line="2017"/>
        <location filename="preset.ui" line="2030"/>
        <location filename="preset.ui" line="2127"/>
        <location filename="preset.ui" line="2140"/>
        <source>50</source>
        <translation>50</translation>
    </message>
    <message>
        <location filename="preset.ui" line="919"/>
        <source>48</source>
        <translation>48</translation>
    </message>
    <message>
        <location filename="preset.ui" line="924"/>
        <source>30</source>
        <translation>30</translation>
    </message>
    <message>
        <location filename="preset.ui" line="929"/>
        <source>29.970</source>
        <translation>29.970</translation>
    </message>
    <message>
        <location filename="preset.ui" line="934"/>
        <source>25</source>
        <translation>25</translation>
    </message>
    <message>
        <location filename="preset.ui" line="939"/>
        <source>24</source>
        <translation>24</translation>
    </message>
    <message>
        <location filename="preset.ui" line="944"/>
        <source>23.976</source>
        <translation>23.976</translation>
    </message>
    <message>
        <location filename="preset.ui" line="949"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="preset.ui" line="954"/>
        <source>18</source>
        <translation>18</translation>
    </message>
    <message>
        <location filename="preset.ui" line="959"/>
        <source>16</source>
        <translation>16</translation>
    </message>
    <message>
        <location filename="preset.ui" line="982"/>
        <source>   Frame rate  </source>
        <translation>Частота кадров  </translation>
    </message>
    <message>
        <location filename="preset.ui" line="1001"/>
        <source>Mode  </source>
        <translation>Режим  </translation>
    </message>
    <message>
        <location filename="preset.ui" line="1032"/>
        <source>Resolution</source>
        <translation>Разрешение</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1088"/>
        <source>7680</source>
        <translation>7680</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1093"/>
        <source>4520</source>
        <translation>4520</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1098"/>
        <source>4096</source>
        <translation>4096</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1103"/>
        <source>3840</source>
        <translation>3840</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1108"/>
        <source>3656</source>
        <translation>3656</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1113"/>
        <source>2048</source>
        <translation>2048</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1118"/>
        <source>1920</source>
        <translation>1920</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1123"/>
        <source>1828</source>
        <translation>1828</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1128"/>
        <source>1440</source>
        <translation>1440</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1133"/>
        <source>1280</source>
        <translation>1280</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1138"/>
        <source>1024</source>
        <translation>1024</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1143"/>
        <location filename="preset.ui" line="1436"/>
        <source>768</source>
        <translation>768</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1148"/>
        <location filename="preset.ui" line="1441"/>
        <source>720</source>
        <translation>720</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1153"/>
        <source>640</source>
        <translation>640</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1158"/>
        <source>320</source>
        <translation>320</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1175"/>
        <source>PAR  </source>
        <translation>PAR  </translation>
    </message>
    <message>
        <location filename="preset.ui" line="1232"/>
        <source>   Aspect ratio  </source>
        <translation>   Соотношение  </translation>
    </message>
    <message>
        <location filename="preset.ui" line="1273"/>
        <source>   Width  </source>
        <translation>Ширина  </translation>
    </message>
    <message>
        <location filename="preset.ui" line="1316"/>
        <source>4320</source>
        <translation>4320</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1321"/>
        <source>3112</source>
        <translation>3112</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1326"/>
        <source>3072</source>
        <translation>3072</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1331"/>
        <source>2664</source>
        <translation>2664</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1336"/>
        <source>2540</source>
        <translation>2540</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1341"/>
        <source>2468</source>
        <translation>2468</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1346"/>
        <source>2304</source>
        <translation>2304</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1351"/>
        <source>2214</source>
        <translation>2214</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1356"/>
        <source>2204</source>
        <translation>2204</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1361"/>
        <source>2160</source>
        <translation>2160</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1366"/>
        <source>2056</source>
        <translation>2056</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1371"/>
        <source>1976</source>
        <translation>1976</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1376"/>
        <source>1744</source>
        <translation>1744</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1381"/>
        <source>1556</source>
        <translation>1556</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1386"/>
        <source>1536</source>
        <translation>1536</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1391"/>
        <source>1332</source>
        <translation>1332</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1396"/>
        <source>1234</source>
        <translation>1234</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1401"/>
        <source>1152</source>
        <translation>1152</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1406"/>
        <source>1107</source>
        <translation>1107</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1411"/>
        <source>1102</source>
        <translation>1102</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1416"/>
        <source>1080</source>
        <translation>1080</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1421"/>
        <source>1028</source>
        <translation>1028</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1426"/>
        <source>988</source>
        <translation>988</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1431"/>
        <source>872</source>
        <translation>872</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1446"/>
        <source>576</source>
        <translation>576</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1451"/>
        <source>540</source>
        <translation>540</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1456"/>
        <source>486</source>
        <translation>486</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1461"/>
        <source>480</source>
        <translation>480</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1466"/>
        <source>240</source>
        <translation>240</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1502"/>
        <source>   Height  </source>
        <translation>Высота  </translation>
    </message>
    <message>
        <location filename="preset.ui" line="1539"/>
        <source>NTSC 4:3</source>
        <translation>NTSC 4:3</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1544"/>
        <source>DV NTSC 3:2</source>
        <translation>DV NTSC 3:2</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1549"/>
        <source>DV PAL 1,25:1</source>
        <translation>DV PAL 1,25:1</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1554"/>
        <source>CCIR 601 NTSC 1,48:1</source>
        <translation>CCIR 601 NTSC 1,48:1</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1559"/>
        <source>CCIR 601 Sq. NTSC 4:3</source>
        <translation>CCIR 601 Sq. NTSC 4:3</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1564"/>
        <source>CCIR 601 Sq. PAL 4:3</source>
        <translation>CCIR 601 Sq. PAL 4:3</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1569"/>
        <source>HDTV 720 16:9</source>
        <translation>HDTV 720 16:9</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1574"/>
        <source>HDTV 1080 16:9</source>
        <translation>HDTV 1080 16:9</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1579"/>
        <source>HDV 1080 16:9</source>
        <translation>HDV 1080 16:9</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1584"/>
        <source>2K Academy 185</source>
        <translation>2K Academy 185</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1589"/>
        <source>2K Academy 178</source>
        <translation>2K Academy 178</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1594"/>
        <source>2K Academy 166</source>
        <translation>2K Academy 166</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1599"/>
        <source>2K Academy 1,37:1</source>
        <translation>2K Academy 1,37:1</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1604"/>
        <source>2K Academy Scope 1,17:1</source>
        <translation>2K Academy Scope 1,17:1</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1609"/>
        <source>2K Super 35 2,35:1</source>
        <translation>2K Super 35 2,35:1</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1614"/>
        <source>2K Super 185</source>
        <translation>2K Super 185</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1619"/>
        <source>2K Super 178</source>
        <translation>2K Super 178</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1624"/>
        <source>2K Super 166</source>
        <translation>2K Super 166</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1629"/>
        <source>2K Full Aperture 4:3</source>
        <translation>2K Full Aperture 4:3</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1634"/>
        <source>4K Academy 185</source>
        <translation>4K Academy 185</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1639"/>
        <source>4K Academy 178</source>
        <translation>4K Academy 178</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1644"/>
        <source>4K Academy 166</source>
        <translation>4K Academy 166</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1649"/>
        <source>4K Academy 1,37:1</source>
        <translation>4K Academy 1,37:1</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1654"/>
        <source>4K Academy Scope 1,17:1</source>
        <translation>4K Academy Scope 1,17:1</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1659"/>
        <source>4K Super 35 2,35:1</source>
        <translation>4K Super 35 2,35:1</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1664"/>
        <source>4K Super 185</source>
        <translation>4K Super 185</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1669"/>
        <source>4K Super 178</source>
        <translation>4K Super 178</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1674"/>
        <source>4K Super 166</source>
        <translation>4K Super 166</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1679"/>
        <source>4K Full Aperture 4:3</source>
        <translation>4K Full Aperture 4:3</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1684"/>
        <source>4K Ultra HD 16:9</source>
        <translation>4K Ultra HD 16:9</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1689"/>
        <source>RED 2540p 16:9</source>
        <translation>RED 2540p 16:9</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1694"/>
        <source>8K Ultra HD 16:9</source>
        <translation>8K Ultra HD 16:9</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1699"/>
        <location filename="preset.ui" line="4079"/>
        <location filename="preset.cpp" line="590"/>
        <location filename="preset.cpp" line="800"/>
        <location filename="preset.cpp" line="869"/>
        <location filename="preset.cpp" line="925"/>
        <location filename="preset.cpp" line="1702"/>
        <source>Custom</source>
        <translation>Пользовательский</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1722"/>
        <source>1:1</source>
        <translation>1:1</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1738"/>
        <source>DAR  </source>
        <translation>DAR  </translation>
    </message>
    <message>
        <location filename="preset.ui" line="1906"/>
        <location filename="preset.ui" line="2207"/>
        <location filename="preset.ui" line="3824"/>
        <location filename="preset.cpp" line="1496"/>
        <location filename="preset.cpp" line="1517"/>
        <location filename="preset.cpp" line="1532"/>
        <location filename="preset.cpp" line="1546"/>
        <source>Bitrate</source>
        <translation>Битрейт</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1953"/>
        <source>Mode</source>
        <translation>Режим</translation>
    </message>
    <message>
        <location filename="preset.ui" line="1988"/>
        <location filename="preset.ui" line="2004"/>
        <location filename="preset.ui" line="2156"/>
        <location filename="preset.cpp" line="1500"/>
        <location filename="preset.cpp" line="1518"/>
        <location filename="preset.cpp" line="1536"/>
        <location filename="preset.cpp" line="1547"/>
        <source>MBps</source>
        <translation>МБ/с</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2046"/>
        <source>MB</source>
        <translation>МБ</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2093"/>
        <location filename="preset.cpp" line="1091"/>
        <location filename="preset.cpp" line="1106"/>
        <location filename="preset.cpp" line="1122"/>
        <location filename="preset.cpp" line="1516"/>
        <source>Constant Bitrate</source>
        <translation>Постоянный битрейт</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2098"/>
        <location filename="preset.cpp" line="1091"/>
        <location filename="preset.cpp" line="1106"/>
        <location filename="preset.cpp" line="1122"/>
        <location filename="preset.cpp" line="1231"/>
        <location filename="preset.cpp" line="1244"/>
        <location filename="preset.cpp" line="1531"/>
        <source>Average Bitrate</source>
        <translation>Средний битрейт</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2103"/>
        <location filename="preset.cpp" line="1091"/>
        <location filename="preset.cpp" line="1106"/>
        <location filename="preset.cpp" line="1122"/>
        <location filename="preset.cpp" line="1143"/>
        <location filename="preset.cpp" line="1165"/>
        <location filename="preset.cpp" line="1182"/>
        <location filename="preset.cpp" line="1198"/>
        <location filename="preset.cpp" line="1215"/>
        <location filename="preset.cpp" line="1438"/>
        <location filename="preset.cpp" line="1545"/>
        <source>Variable Bitrate</source>
        <translation>Переменный битрейт</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2108"/>
        <location filename="preset.cpp" line="1092"/>
        <location filename="preset.cpp" line="1107"/>
        <location filename="preset.cpp" line="1123"/>
        <location filename="preset.cpp" line="1231"/>
        <location filename="preset.cpp" line="1244"/>
        <location filename="preset.cpp" line="1562"/>
        <source>Constant Rate Factor</source>
        <translation>Постоянный RF</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2113"/>
        <location filename="preset.cpp" line="1092"/>
        <location filename="preset.cpp" line="1107"/>
        <location filename="preset.cpp" line="1123"/>
        <location filename="preset.cpp" line="1576"/>
        <source>Constant QP</source>
        <translation>Постоянный QP</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2232"/>
        <source>Maxrate</source>
        <translation>Maxrate</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2257"/>
        <source>Bufsize</source>
        <translation>Буфер</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2282"/>
        <source>Minrate</source>
        <translation>Minrate</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2313"/>
        <location filename="preset.ui" line="2884"/>
        <location filename="preset.ui" line="3609"/>
        <source>Codec</source>
        <translation>Кодек</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2372"/>
        <source>Container</source>
        <translation>Контейнер</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2454"/>
        <source>MKV</source>
        <translation>MKV</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2459"/>
        <source>MOV</source>
        <translation>MOV</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2464"/>
        <source>MP4</source>
        <translation>MP4</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2515"/>
        <location filename="preset.cpp" line="1088"/>
        <source>H.265/HEVC 4:2:0 10 bit</source>
        <translation>H.265/HEVC 4:2:0 10 бит</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2520"/>
        <location filename="preset.cpp" line="1103"/>
        <source>H.265/HEVC 4:2:0 8 bit</source>
        <translation>H.265/HEVC 4:2:0 8 бит</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2525"/>
        <location filename="preset.cpp" line="1119"/>
        <source>H.264/AVC 4:2:0 8 bit</source>
        <translation>H.264/AVC 4:2:0 8 бит</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2530"/>
        <location filename="preset.cpp" line="1135"/>
        <source>Intel QSV H.264/AVC 4:2:0 8 bit</source>
        <translation>Intel QSV H.264/AVC 4:2:0 8 бит</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2535"/>
        <location filename="preset.cpp" line="1157"/>
        <source>Intel QSV MPEG-2 4:2:0 8 bit</source>
        <translation>Intel QSV MPEG-2 4:2:0 8 бит</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2540"/>
        <location filename="preset.cpp" line="1179"/>
        <source>NVENC H.265/HEVC 4:2:0 10 bit</source>
        <translation>NVENC H.265/HEVC 4:2:0 10 бит</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2545"/>
        <location filename="preset.cpp" line="1195"/>
        <source>NVENC H.265/HEVC 4:2:0 8 bit</source>
        <translation>NVENC H.265/HEVC 4:2:0 8 бит</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2550"/>
        <location filename="preset.cpp" line="1212"/>
        <source>NVENC H.264/AVC 4:2:0 8 bit</source>
        <translation>NVENC H.264/AVC 4:2:0 8 бит</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2555"/>
        <location filename="preset.cpp" line="1229"/>
        <source>VP9 4:2:0 10 bit</source>
        <translation>VP9 4:2:0 10 бит</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2560"/>
        <location filename="preset.cpp" line="1242"/>
        <source>VP9 4:2:0 8 bit</source>
        <translation>VP9 4:2:0 8 бит</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2565"/>
        <location filename="preset.cpp" line="1256"/>
        <source>ProRes Proxy</source>
        <translation>ProRes Proxy</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2570"/>
        <source>ProRes LT</source>
        <translation>ProRes LT</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2575"/>
        <location filename="preset.cpp" line="1288"/>
        <source>ProRes Standard</source>
        <translation>ProRes Standard</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2580"/>
        <source>ProRes HQ</source>
        <translation>ProRes HQ</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2585"/>
        <source>ProRes 4444</source>
        <translation>ProRes 4444</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2590"/>
        <source>ProRes 4444 XQ</source>
        <translation>ProRes 4444 XQ</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2595"/>
        <source>DNxHR LB</source>
        <translation>DNxHR LB</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2600"/>
        <source>DNxHR SQ</source>
        <translation>DNxHR SQ</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2605"/>
        <source>DNxHR HQ</source>
        <translation>DNxHR HQ</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2610"/>
        <source>DNxHR HQX</source>
        <translation>DNxHR HQX</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2615"/>
        <source>DNxHR 444</source>
        <translation>DNxHR 444</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2620"/>
        <source>XDCAM HD422</source>
        <translation>XDCAM HD422</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2648"/>
        <source>   Pass</source>
        <translation>Проход</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2701"/>
        <source>High</source>
        <translation>High</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2706"/>
        <source>Main</source>
        <translation>Main</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2711"/>
        <source>Main10</source>
        <translation>Main10</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2716"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2721"/>
        <location filename="preset.ui" line="3226"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2726"/>
        <location filename="preset.ui" line="3231"/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2731"/>
        <location filename="preset.ui" line="3241"/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2736"/>
        <location filename="preset.ui" line="3251"/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2741"/>
        <location filename="preset.ui" line="3261"/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2746"/>
        <source>dnxhr_lb</source>
        <translation>dnxhr_lb</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2751"/>
        <source>dnxhr_sq</source>
        <translation>dnxhr_sq</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2756"/>
        <source>dnxhr_hq</source>
        <translation>dnxhr_hq</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2761"/>
        <source>dnxhr_hqx</source>
        <translation>dnxhr_hqx</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2766"/>
        <source>dnxhr_444</source>
        <translation>dnxhr_444</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2771"/>
        <location filename="preset.ui" line="3170"/>
        <location filename="preset.ui" line="3221"/>
        <location filename="preset.cpp" line="1050"/>
        <location filename="preset.cpp" line="1052"/>
        <location filename="preset.cpp" line="1144"/>
        <location filename="preset.cpp" line="1166"/>
        <location filename="preset.cpp" line="1170"/>
        <location filename="preset.cpp" line="1237"/>
        <location filename="preset.cpp" line="1250"/>
        <location filename="preset.cpp" line="1258"/>
        <location filename="preset.cpp" line="1259"/>
        <location filename="preset.cpp" line="1262"/>
        <location filename="preset.cpp" line="1274"/>
        <location filename="preset.cpp" line="1275"/>
        <location filename="preset.cpp" line="1278"/>
        <location filename="preset.cpp" line="1290"/>
        <location filename="preset.cpp" line="1291"/>
        <location filename="preset.cpp" line="1294"/>
        <location filename="preset.cpp" line="1306"/>
        <location filename="preset.cpp" line="1307"/>
        <location filename="preset.cpp" line="1310"/>
        <location filename="preset.cpp" line="1322"/>
        <location filename="preset.cpp" line="1323"/>
        <location filename="preset.cpp" line="1326"/>
        <location filename="preset.cpp" line="1338"/>
        <location filename="preset.cpp" line="1339"/>
        <location filename="preset.cpp" line="1342"/>
        <location filename="preset.cpp" line="1354"/>
        <location filename="preset.cpp" line="1355"/>
        <location filename="preset.cpp" line="1358"/>
        <location filename="preset.cpp" line="1371"/>
        <location filename="preset.cpp" line="1372"/>
        <location filename="preset.cpp" line="1375"/>
        <location filename="preset.cpp" line="1388"/>
        <location filename="preset.cpp" line="1389"/>
        <location filename="preset.cpp" line="1392"/>
        <location filename="preset.cpp" line="1405"/>
        <location filename="preset.cpp" line="1406"/>
        <location filename="preset.cpp" line="1409"/>
        <location filename="preset.cpp" line="1421"/>
        <location filename="preset.cpp" line="1422"/>
        <location filename="preset.cpp" line="1425"/>
        <location filename="preset.cpp" line="1439"/>
        <location filename="preset.cpp" line="1462"/>
        <location filename="preset.cpp" line="1463"/>
        <location filename="preset.cpp" line="1466"/>
        <location filename="preset.cpp" line="1495"/>
        <location filename="preset.cpp" line="1507"/>
        <location filename="preset.cpp" line="1508"/>
        <location filename="preset.cpp" line="1509"/>
        <location filename="preset.cpp" line="1510"/>
        <location filename="preset.cpp" line="1658"/>
        <location filename="preset.cpp" line="1662"/>
        <location filename="preset.cpp" line="1666"/>
        <source>Auto</source>
        <translation>Авто</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2831"/>
        <location filename="preset.cpp" line="672"/>
        <location filename="preset.cpp" line="673"/>
        <location filename="preset.cpp" line="674"/>
        <location filename="preset.cpp" line="680"/>
        <location filename="preset.cpp" line="681"/>
        <location filename="preset.cpp" line="1093"/>
        <location filename="preset.cpp" line="1108"/>
        <location filename="preset.cpp" line="1124"/>
        <location filename="preset.cpp" line="1232"/>
        <location filename="preset.cpp" line="1245"/>
        <source>1 Pass</source>
        <translation>1 Проход</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2836"/>
        <location filename="preset.cpp" line="672"/>
        <location filename="preset.cpp" line="673"/>
        <location filename="preset.cpp" line="674"/>
        <location filename="preset.cpp" line="677"/>
        <location filename="preset.cpp" line="678"/>
        <location filename="preset.cpp" line="679"/>
        <location filename="preset.cpp" line="680"/>
        <location filename="preset.cpp" line="681"/>
        <location filename="preset.cpp" line="1093"/>
        <location filename="preset.cpp" line="1108"/>
        <location filename="preset.cpp" line="1124"/>
        <location filename="preset.cpp" line="1232"/>
        <location filename="preset.cpp" line="1245"/>
        <source>2 Pass</source>
        <translation>2 Прохода</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2909"/>
        <source>   Profile</source>
        <translation>Проф.</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2959"/>
        <location filename="preset.ui" line="4010"/>
        <location filename="preset.ui" line="4118"/>
        <location filename="preset.cpp" line="640"/>
        <location filename="preset.cpp" line="641"/>
        <location filename="preset.cpp" line="642"/>
        <location filename="preset.cpp" line="643"/>
        <location filename="preset.cpp" line="644"/>
        <location filename="preset.cpp" line="645"/>
        <location filename="preset.cpp" line="646"/>
        <location filename="preset.cpp" line="647"/>
        <location filename="preset.cpp" line="665"/>
        <location filename="preset.cpp" line="1054"/>
        <location filename="preset.cpp" line="1056"/>
        <location filename="preset.cpp" line="1058"/>
        <location filename="preset.cpp" line="1060"/>
        <location filename="preset.cpp" line="1185"/>
        <location filename="preset.cpp" line="1201"/>
        <location filename="preset.cpp" line="1218"/>
        <location filename="preset.cpp" line="1234"/>
        <location filename="preset.cpp" line="1247"/>
        <location filename="preset.cpp" line="1261"/>
        <location filename="preset.cpp" line="1277"/>
        <location filename="preset.cpp" line="1293"/>
        <location filename="preset.cpp" line="1309"/>
        <location filename="preset.cpp" line="1325"/>
        <location filename="preset.cpp" line="1341"/>
        <location filename="preset.cpp" line="1357"/>
        <location filename="preset.cpp" line="1374"/>
        <location filename="preset.cpp" line="1391"/>
        <location filename="preset.cpp" line="1408"/>
        <location filename="preset.cpp" line="1424"/>
        <location filename="preset.cpp" line="1441"/>
        <location filename="preset.cpp" line="1465"/>
        <source>None</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2964"/>
        <location filename="preset.cpp" line="640"/>
        <location filename="preset.cpp" line="641"/>
        <location filename="preset.cpp" line="642"/>
        <location filename="preset.cpp" line="1054"/>
        <location filename="preset.cpp" line="1056"/>
        <source>Ultrafast</source>
        <translation>Ultrafast</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2969"/>
        <location filename="preset.cpp" line="640"/>
        <location filename="preset.cpp" line="641"/>
        <location filename="preset.cpp" line="642"/>
        <location filename="preset.cpp" line="1054"/>
        <location filename="preset.cpp" line="1056"/>
        <source>Superfast</source>
        <translation>Superfast</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2974"/>
        <location filename="preset.cpp" line="640"/>
        <location filename="preset.cpp" line="641"/>
        <location filename="preset.cpp" line="642"/>
        <location filename="preset.cpp" line="643"/>
        <location filename="preset.cpp" line="644"/>
        <location filename="preset.cpp" line="1054"/>
        <location filename="preset.cpp" line="1056"/>
        <location filename="preset.cpp" line="1058"/>
        <location filename="preset.cpp" line="1060"/>
        <source>Veryfast</source>
        <translation>Veryfast</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2979"/>
        <location filename="preset.cpp" line="640"/>
        <location filename="preset.cpp" line="641"/>
        <location filename="preset.cpp" line="642"/>
        <location filename="preset.cpp" line="643"/>
        <location filename="preset.cpp" line="644"/>
        <location filename="preset.cpp" line="1054"/>
        <location filename="preset.cpp" line="1056"/>
        <location filename="preset.cpp" line="1058"/>
        <location filename="preset.cpp" line="1060"/>
        <source>Faster</source>
        <translation>Faster</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2984"/>
        <location filename="preset.cpp" line="640"/>
        <location filename="preset.cpp" line="641"/>
        <location filename="preset.cpp" line="642"/>
        <location filename="preset.cpp" line="643"/>
        <location filename="preset.cpp" line="644"/>
        <location filename="preset.cpp" line="1055"/>
        <location filename="preset.cpp" line="1057"/>
        <location filename="preset.cpp" line="1058"/>
        <location filename="preset.cpp" line="1060"/>
        <source>Fast</source>
        <translation>Fast</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2989"/>
        <location filename="preset.cpp" line="640"/>
        <location filename="preset.cpp" line="641"/>
        <location filename="preset.cpp" line="642"/>
        <location filename="preset.cpp" line="643"/>
        <location filename="preset.cpp" line="644"/>
        <location filename="preset.cpp" line="1055"/>
        <location filename="preset.cpp" line="1057"/>
        <location filename="preset.cpp" line="1058"/>
        <location filename="preset.cpp" line="1060"/>
        <source>Medium</source>
        <translation>Medium</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2994"/>
        <location filename="preset.cpp" line="640"/>
        <location filename="preset.cpp" line="641"/>
        <location filename="preset.cpp" line="642"/>
        <location filename="preset.cpp" line="643"/>
        <location filename="preset.cpp" line="644"/>
        <location filename="preset.cpp" line="645"/>
        <location filename="preset.cpp" line="646"/>
        <location filename="preset.cpp" line="647"/>
        <location filename="preset.cpp" line="1055"/>
        <location filename="preset.cpp" line="1057"/>
        <location filename="preset.cpp" line="1059"/>
        <location filename="preset.cpp" line="1061"/>
        <location filename="preset.cpp" line="1185"/>
        <location filename="preset.cpp" line="1201"/>
        <location filename="preset.cpp" line="1218"/>
        <source>Slow</source>
        <translation>Slow</translation>
    </message>
    <message>
        <location filename="preset.ui" line="2999"/>
        <location filename="preset.cpp" line="640"/>
        <location filename="preset.cpp" line="641"/>
        <location filename="preset.cpp" line="642"/>
        <location filename="preset.cpp" line="643"/>
        <location filename="preset.cpp" line="644"/>
        <location filename="preset.cpp" line="1055"/>
        <location filename="preset.cpp" line="1057"/>
        <location filename="preset.cpp" line="1059"/>
        <location filename="preset.cpp" line="1061"/>
        <source>Slower</source>
        <translation>Slower</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3004"/>
        <location filename="preset.cpp" line="640"/>
        <location filename="preset.cpp" line="641"/>
        <location filename="preset.cpp" line="642"/>
        <location filename="preset.cpp" line="643"/>
        <location filename="preset.cpp" line="644"/>
        <location filename="preset.cpp" line="1055"/>
        <location filename="preset.cpp" line="1057"/>
        <location filename="preset.cpp" line="1059"/>
        <location filename="preset.cpp" line="1061"/>
        <source>Veryslow</source>
        <oldsource>Very slow</oldsource>
        <translation>Veryslow</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3036"/>
        <source>Advanced</source>
        <translation>Дополнительно</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3087"/>
        <source>   Pix fmt</source>
        <translation>   Pix fmt</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3140"/>
        <source>yuv444p10le</source>
        <translation>yuv444p10le</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3145"/>
        <source>yuv422p10le</source>
        <translation>yuv422p10le</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3150"/>
        <source>yuv420p10le</source>
        <translation>yuv420p10le</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3155"/>
        <source>yuv422p</source>
        <translation>yuv422p</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3160"/>
        <source>yuv420p</source>
        <translation>yuv420p</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3165"/>
        <source>p010le</source>
        <translation>p010le</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3236"/>
        <source>2.1</source>
        <translation>2.1</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3246"/>
        <source>3.1</source>
        <translation>3.1</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3256"/>
        <source>4.1</source>
        <translation>4.1</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3266"/>
        <source>5.1</source>
        <translation>5.1</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3271"/>
        <source>5.2</source>
        <translation>5.2</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3276"/>
        <source>6</source>
        <translation>6</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3281"/>
        <source>6.1</source>
        <translation>6.1</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3286"/>
        <source>6.2</source>
        <translation>6.2</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3309"/>
        <source>Level</source>
        <translation>Уровень</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3507"/>
        <source>8000</source>
        <translation>8000</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3512"/>
        <source>11025</source>
        <translation>11025</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3517"/>
        <source>16000</source>
        <translation>16000</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3522"/>
        <source>22050</source>
        <translation>22050</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3527"/>
        <source>32000</source>
        <translation>32000</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3532"/>
        <source>44100</source>
        <translation>44100</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3537"/>
        <source>48000</source>
        <translation>48000</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3542"/>
        <source>88200</source>
        <translation>88200</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3547"/>
        <source>96000</source>
        <translation>96000</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3552"/>
        <source>176400</source>
        <translation>176400</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3557"/>
        <source>192000</source>
        <translation>192000</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3669"/>
        <source>AAC</source>
        <translation>AAC</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3674"/>
        <source>AC3</source>
        <translation>AC3</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3679"/>
        <source>DTS</source>
        <translation>DTS</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3701"/>
        <source>Channels</source>
        <translation>Каналы</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3751"/>
        <source>384k</source>
        <translation>384k</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3756"/>
        <source>320k</source>
        <translation>320k</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3761"/>
        <source>256k</source>
        <translation>256k</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3766"/>
        <source>192k</source>
        <translation>192k</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3771"/>
        <source>128k</source>
        <translation>128k</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3776"/>
        <source>96k</source>
        <translation>96k</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3799"/>
        <source>Sample rate</source>
        <translation>Частота дискр.</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3852"/>
        <source>trim to 1 ch</source>
        <translation>обрезка до 1 ch</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3857"/>
        <source>trim to 2 ch</source>
        <translation>обрезка до 2 ch</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3891"/>
        <source>HDR Metadata</source>
        <translation>HDR Метаданные</translation>
    </message>
    <message>
        <location filename="preset.ui" line="3972"/>
        <source>Master Display</source>
        <translation>Мастер Дисплей</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4059"/>
        <location filename="preset.cpp" line="1706"/>
        <source>Display P3</source>
        <translation>Display P3</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4064"/>
        <location filename="preset.cpp" line="1712"/>
        <source>DCI P3</source>
        <translation>DCI P3</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4069"/>
        <source>BT.2020</source>
        <translation>BT.2020</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4074"/>
        <source>BT.709</source>
        <translation>BT.709</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4149"/>
        <source>Chroma</source>
        <translation>Хрома</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4183"/>
        <source>White</source>
        <translation>Белый</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4208"/>
        <source>Display</source>
        <translation>Дисплей</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4252"/>
        <source>Luminance</source>
        <translation>Яркость</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4312"/>
        <source>Min lum</source>
        <translation>Min lum</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4340"/>
        <source>Max cll</source>
        <translation>Max cll</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4403"/>
        <source>Max fall</source>
        <translation>Max fall</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4494"/>
        <source>Max lum</source>
        <translation>Max lum</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4538"/>
        <source>Color</source>
        <translation>Цвет</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4573"/>
        <source>Matrix</source>
        <translation>Матрица</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4634"/>
        <location filename="preset.ui" line="4781"/>
        <location filename="preset.ui" line="5025"/>
        <source>bt470bg</source>
        <translation>bt470bg</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4639"/>
        <location filename="preset.ui" line="4786"/>
        <location filename="preset.ui" line="5030"/>
        <source>bt709</source>
        <translation>bt709</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4644"/>
        <source>bt2020nc</source>
        <translation>bt2020nc</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4649"/>
        <source>bt2020c</source>
        <translation>bt2020c</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4654"/>
        <location filename="preset.ui" line="4796"/>
        <location filename="preset.ui" line="5050"/>
        <source>smpte170m</source>
        <translation>smpte170m</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4659"/>
        <location filename="preset.ui" line="4801"/>
        <location filename="preset.ui" line="5055"/>
        <source>smpte240m</source>
        <translation>smpte240m</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4664"/>
        <source>smpte2085</source>
        <translation>smpte2085</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4669"/>
        <source>chroma-derived-nc</source>
        <translation>chroma-derived-nc</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4674"/>
        <source>chroma-derived-c</source>
        <translation>chroma-derived-c</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4679"/>
        <source>fcc</source>
        <translation>fcc</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4684"/>
        <source>GBR</source>
        <translation>GBR</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4689"/>
        <source>ICtCp</source>
        <translation>ICtCp</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4694"/>
        <source>YCgCo</source>
        <translation>YCgCo</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4702"/>
        <location filename="preset.ui" line="5103"/>
        <location filename="preset.ui" line="5110"/>
        <source>Convert</source>
        <translation>Преобр.</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4724"/>
        <source>Primaries</source>
        <translation>Первичн.</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4776"/>
        <location filename="preset.ui" line="5020"/>
        <source>bt470m</source>
        <translation>bt470m</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4791"/>
        <source>bt2020</source>
        <translation>bt2020</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4806"/>
        <location filename="preset.ui" line="5060"/>
        <source>smpte428</source>
        <translation>smpte428</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4811"/>
        <source>smpte431</source>
        <translation>smpte431</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4816"/>
        <source>smpte432</source>
        <translation>smpte432</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4821"/>
        <source>film</source>
        <translation>film</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4857"/>
        <source>Range</source>
        <translation>Диапазон</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4918"/>
        <source>Full</source>
        <translation>Full</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4923"/>
        <source>Limited</source>
        <translation>Limited</translation>
    </message>
    <message>
        <location filename="preset.ui" line="4965"/>
        <source>Transfer</source>
        <translation>Перемещ.</translation>
    </message>
    <message>
        <location filename="preset.ui" line="5035"/>
        <source>bt1361e</source>
        <translation>bt1361e</translation>
    </message>
    <message>
        <location filename="preset.ui" line="5040"/>
        <source>bt2020-10</source>
        <translation>bt2020-10</translation>
    </message>
    <message>
        <location filename="preset.ui" line="5045"/>
        <source>bt2020-12</source>
        <translation>bt2020-12</translation>
    </message>
    <message>
        <location filename="preset.ui" line="5065"/>
        <source>smpte2084</source>
        <translation>smpte2084</translation>
    </message>
    <message>
        <location filename="preset.ui" line="5070"/>
        <source>arib-std-b67</source>
        <translation>arib-std-b67</translation>
    </message>
    <message>
        <location filename="preset.ui" line="5075"/>
        <source>linear</source>
        <translation>linear</translation>
    </message>
    <message>
        <location filename="preset.ui" line="5080"/>
        <source>log100</source>
        <translation>log100</translation>
    </message>
    <message>
        <location filename="preset.ui" line="5085"/>
        <source>log316</source>
        <translation>log316</translation>
    </message>
    <message>
        <location filename="preset.ui" line="5090"/>
        <source>iec61966-2-1</source>
        <translation>iec61966-2-1</translation>
    </message>
    <message>
        <location filename="preset.ui" line="5095"/>
        <source>iec61966-2-4</source>
        <translation>iec61966-2-4</translation>
    </message>
    <message>
        <location filename="preset.cpp" line="564"/>
        <source>ProRes Proxy, </source>
        <translation>ProRes Proxy, </translation>
    </message>
    <message>
        <location filename="preset.cpp" line="566"/>
        <source>ProRes Standard, </source>
        <translation>ProRes Standard, </translation>
    </message>
    <message>
        <location filename="preset.cpp" line="576"/>
        <source>From source, </source>
        <translation>Копия, </translation>
    </message>
    <message>
        <location filename="preset.cpp" line="634"/>
        <source> MBps, </source>
        <translation> МБ/с, </translation>
    </message>
    <message>
        <location filename="preset.cpp" line="666"/>
        <source>Preset: </source>
        <translation>Пресет: </translation>
    </message>
    <message>
        <location filename="preset.cpp" line="699"/>
        <source>Enabled, </source>
        <translation>Включено, </translation>
    </message>
    <message>
        <location filename="preset.cpp" line="730"/>
        <source>Audio: </source>
        <translation>Аудио: </translation>
    </message>
    <message>
        <location filename="preset.cpp" line="554"/>
        <location filename="preset.cpp" line="559"/>
        <location filename="preset.cpp" line="562"/>
        <source>YUV, 4:2:0, 10 bit, </source>
        <translation>YUV, 4:2:0, 10 бит, </translation>
    </message>
    <message>
        <location filename="preset.cpp" line="555"/>
        <location filename="preset.cpp" line="556"/>
        <location filename="preset.cpp" line="557"/>
        <location filename="preset.cpp" line="558"/>
        <location filename="preset.cpp" line="560"/>
        <location filename="preset.cpp" line="561"/>
        <source>YUV, 4:2:0, 8 bit, </source>
        <translation>YUV, 4:2:0, 8 бит, </translation>
    </message>
    <message>
        <location filename="preset.cpp" line="563"/>
        <source>YUV, 4:2:0, 8  bit, </source>
        <translation>YUV, 4:2:0, 8  бит, </translation>
    </message>
    <message>
        <location filename="preset.cpp" line="564"/>
        <location filename="preset.cpp" line="565"/>
        <location filename="preset.cpp" line="566"/>
        <location filename="preset.cpp" line="567"/>
        <location filename="preset.cpp" line="573"/>
        <source>YUV, 4:2:2, 10 bit, </source>
        <translation>YUV, 4:2:2, 10 бит, </translation>
    </message>
    <message>
        <location filename="preset.cpp" line="568"/>
        <location filename="preset.cpp" line="569"/>
        <location filename="preset.cpp" line="574"/>
        <source>YUV, 4:4:4, 10 bit, </source>
        <translation>YUV, 4:4:4, 10 бит, </translation>
    </message>
    <message>
        <location filename="preset.cpp" line="570"/>
        <location filename="preset.cpp" line="571"/>
        <location filename="preset.cpp" line="572"/>
        <location filename="preset.cpp" line="575"/>
        <source>YUV, 4:2:2, 8 bit, </source>
        <translation>YUV, 4:2:2, 8 бит, </translation>
    </message>
    <message>
        <location filename="preset.cpp" line="960"/>
        <location filename="preset.cpp" line="963"/>
        <source>Undef</source>
        <translation>Неопред.</translation>
    </message>
    <message>
        <location filename="preset.cpp" line="1183"/>
        <location filename="preset.cpp" line="1199"/>
        <location filename="preset.cpp" line="1216"/>
        <source>2 Pass Optimisation</source>
        <translation>2 Прох. оптимизация</translation>
    </message>
    <message>
        <location filename="preset.cpp" line="1563"/>
        <source>Rate factor</source>
        <translation>Фактор RF</translation>
    </message>
    <message>
        <location filename="preset.cpp" line="1577"/>
        <source>Quantizer</source>
        <translation>Квантов.</translation>
    </message>
    <message>
        <location filename="preset.cpp" line="1690"/>
        <location filename="preset.cpp" line="1691"/>
        <location filename="preset.cpp" line="1692"/>
        <source>Unsprt</source>
        <translation>Неподдерж.</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="settings.ui" line="35"/>
        <location filename="settings.ui" line="1608"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="settings.ui" line="179"/>
        <source>Paths and Files</source>
        <translation>Папки и файлы</translation>
    </message>
    <message>
        <location filename="settings.ui" line="201"/>
        <location filename="settings.ui" line="850"/>
        <source>Advanced</source>
        <translation>Дополнительно</translation>
    </message>
    <message>
        <location filename="settings.ui" line="236"/>
        <source>General</source>
        <translation>Основное</translation>
    </message>
    <message>
        <location filename="settings.ui" line="281"/>
        <source>Files</source>
        <translation>Файлы</translation>
    </message>
    <message>
        <location filename="settings.ui" line="320"/>
        <source>  Prefix</source>
        <translation>Префикс</translation>
    </message>
    <message>
        <location filename="settings.ui" line="343"/>
        <source>Current filename</source>
        <translation>Текущее имя файла</translation>
    </message>
    <message>
        <location filename="settings.ui" line="348"/>
        <location filename="settings.ui" line="401"/>
        <source>Custom filename</source>
        <translation>Польз. имя файла</translation>
    </message>
    <message>
        <location filename="settings.ui" line="359"/>
        <source>  Filename</source>
        <translation>Имя файла</translation>
    </message>
    <message>
        <location filename="settings.ui" line="414"/>
        <source>Custom prefix</source>
        <translation>Польз. префикс</translation>
    </message>
    <message>
        <location filename="settings.ui" line="451"/>
        <source>  Overwrite existing files</source>
        <translation>Переписывать сущ. файлы</translation>
    </message>
    <message>
        <location filename="settings.ui" line="479"/>
        <location filename="settings.cpp" line="658"/>
        <source>None</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="settings.ui" line="502"/>
        <source>By name and index</source>
        <translation>По имени и индексу</translation>
    </message>
    <message>
        <location filename="settings.ui" line="507"/>
        <source>By time and date</source>
        <translation>По времени и дате</translation>
    </message>
    <message>
        <location filename="settings.ui" line="584"/>
        <source>Paths</source>
        <translation>Папки</translation>
    </message>
    <message>
        <location filename="settings.ui" line="641"/>
        <location filename="settings.ui" line="753"/>
        <source>Default (In the same folder as the file)</source>
        <translation>По умолчанию (та же папка)</translation>
    </message>
    <message>
        <location filename="settings.ui" line="673"/>
        <source>  Temporary path</source>
        <translation>Временная папка</translation>
    </message>
    <message>
        <location filename="settings.ui" line="686"/>
        <source>  Output  path</source>
        <translation>Выходная папка</translation>
    </message>
    <message>
        <location filename="settings.ui" line="722"/>
        <source>Select temp folder</source>
        <translation>Выбрать временную папку</translation>
    </message>
    <message>
        <location filename="settings.ui" line="786"/>
        <source>Select output folder</source>
        <translation>Выбрать выходную папку</translation>
    </message>
    <message>
        <location filename="settings.ui" line="895"/>
        <source>View and theme</source>
        <translation>Вид и тема</translation>
    </message>
    <message>
        <location filename="settings.ui" line="944"/>
        <source>Size</source>
        <translation>Разм</translation>
    </message>
    <message>
        <location filename="settings.ui" line="976"/>
        <source>Font</source>
        <translation>Шрифт</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1100"/>
        <source>  Show HDR info</source>
        <translation>Показать инфо HDR</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1183"/>
        <source>  Minimize in tray</source>
        <translation>Сворачивать в трей</translation>
    </message>
    <message>
        <location filename="settings.ui" line="992"/>
        <source>   Theme</source>
        <translation>          Тема</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1059"/>
        <source>Gray</source>
        <translation>Gray</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1064"/>
        <source>Dark</source>
        <translation>Dark</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1069"/>
        <source>Wave</source>
        <translation>Wave</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1074"/>
        <source>Default</source>
        <translation>По умолчанию</translation>
    </message>
    <message>
        <location filename="settings.ui" line="921"/>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1209"/>
        <source>8</source>
        <translation>8</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1214"/>
        <source>9</source>
        <translation>9</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1219"/>
        <source>10</source>
        <translation>10</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1224"/>
        <source>11</source>
        <translation>11</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1229"/>
        <source>12</source>
        <translation>12</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1256"/>
        <source>English</source>
        <translation>Английский</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1261"/>
        <source>Chinese</source>
        <translation>Китайский</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1266"/>
        <source>German</source>
        <translation>Немецкий</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1271"/>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1313"/>
        <source>Overheating protection</source>
        <translation>Защита от перегрева</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1395"/>
        <source>      Every</source>
        <translation>   Каждые</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1405"/>
        <source>sec</source>
        <translation>сек</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1433"/>
        <source>Enable overheating protection (for 25 sec)</source>
        <translation>Включить защиту от перегрева</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1592"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1681"/>
        <source>Reset</source>
        <translation>Сброс</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1725"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="settings.ui" line="1759"/>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="275"/>
        <source>Settings file not found!
</source>
        <translation>Файл настроек не найден!</translation>
    </message>
</context>
<context>
    <name>Taskcomplete</name>
    <message>
        <location filename="taskcomplete.ui" line="35"/>
        <source>Task</source>
        <translation>Задача</translation>
    </message>
    <message>
        <location filename="taskcomplete.ui" line="196"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="taskcomplete.ui" line="238"/>
        <source>Cine Encoder</source>
        <translation>Cine Encoder</translation>
    </message>
    <message>
        <location filename="taskcomplete.ui" line="300"/>
        <source>Ok</source>
        <translation>Ок</translation>
    </message>
    <message>
        <location filename="taskcomplete.ui" line="346"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p align=&quot;center&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Noto Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="taskcomplete.cpp" line="96"/>
        <source>Pause

 Resume after: </source>
        <translation>Пауза

Продолжение через: </translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Форма</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Cine Encoder</source>
        <translation>Cine Encoder</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="156"/>
        <source>Progress:</source>
        <translation>Прогресс:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="223"/>
        <source>%p%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="287"/>
        <source>Remaining:</source>
        <translation>Осталось:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="300"/>
        <source>00:00:00</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="347"/>
        <location filename="mainwindow.cpp" line="362"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="476"/>
        <location filename="mainwindow.cpp" line="439"/>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="507"/>
        <location filename="mainwindow.ui" line="7180"/>
        <location filename="mainwindow.ui" line="7186"/>
        <location filename="mainwindow.cpp" line="440"/>
        <source>Edit</source>
        <translation>Правка</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="564"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="608"/>
        <location filename="mainwindow.cpp" line="433"/>
        <source>Remove</source>
        <translation>Убрать</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="647"/>
        <source>All</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="652"/>
        <source>CPU Only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="657"/>
        <source>NVENC Only</source>
        <translation>Только NVENC</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="662"/>
        <source>IntelQSV Only</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="787"/>
        <source>      Presets</source>
        <translation>     Пресеты</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="801"/>
        <location filename="mainwindow.ui" line="5150"/>
        <source>Mode</source>
        <translation>Режим</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="815"/>
        <source>Rate</source>
        <translation>Частота</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="829"/>
        <source>Preset</source>
        <translation>Пресет</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="843"/>
        <source>Pass</source>
        <translation>Проход</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="857"/>
        <location filename="mainwindow.ui" line="1278"/>
        <source>Audio</source>
        <translation>Аудио</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="871"/>
        <source>Container</source>
        <translation>Контейнер</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1000"/>
        <source>ID 23 - TECHNICAL INFO: Minrate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1005"/>
        <source>ID 24 - TECHNICAL INFO: Level</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1010"/>
        <source>ID 25 - TECHNICAL INFO: Asample Rate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1015"/>
        <source>ID 26 - TECHNICAL INFO: Achannels</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1020"/>
        <source>ID 27 - TECHNICAL INFO: Matrix</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1025"/>
        <source>ID 28 - TECHNICAL INFO: Primary</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1030"/>
        <source>ID 29 - TECHNICAL INFO: TRC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1035"/>
        <source>ID 30 - TECHNICAL INFO: Preset Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1040"/>
        <source>ID 31 - TECHNICAL INFO: REP_Primary</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1045"/>
        <source>ID 32 - TECHNICAL INFO: REP_Matrix</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1050"/>
        <source>ID 33 - TECHNICAL INFO: REP_TRC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3114"/>
        <location filename="mainwindow.ui" line="6906"/>
        <location filename="mainwindow.ui" line="6909"/>
        <source>View</source>
        <translation>Вид</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3118"/>
        <source>Simple View</source>
        <translation>Простой вид</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3123"/>
        <source>Extended Viev</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="104"/>
        <source>Metadata</source>
        <translation>Метаданные</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5852"/>
        <source>   Performer:</source>
        <translation>  Исполнитель:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5593"/>
        <source>   Author:</source>
        <translation>   Автор:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5710"/>
        <source>   Description:</source>
        <translation>  Описание:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5553"/>
        <source>   Year:</source>
        <translation>   Год:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5670"/>
        <source>   Title:</source>
        <translation>   Название:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5992"/>
        <source>   Name:</source>
        <translation>   Имя:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1413"/>
        <source>Audio #7:</source>
        <translation>Аудио #7:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1919"/>
        <source>Audio #3:</source>
        <translation>Аудио #3:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1397"/>
        <location filename="mainwindow.ui" line="1508"/>
        <location filename="mainwindow.ui" line="1542"/>
        <location filename="mainwindow.ui" line="1576"/>
        <location filename="mainwindow.ui" line="1696"/>
        <location filename="mainwindow.ui" line="1874"/>
        <location filename="mainwindow.ui" line="1972"/>
        <location filename="mainwindow.ui" line="2048"/>
        <location filename="mainwindow.ui" line="2082"/>
        <location filename="mainwindow.ui" line="2307"/>
        <location filename="mainwindow.ui" line="2373"/>
        <location filename="mainwindow.ui" line="2554"/>
        <location filename="mainwindow.ui" line="2588"/>
        <location filename="mainwindow.ui" line="2696"/>
        <location filename="mainwindow.ui" line="2775"/>
        <location filename="mainwindow.ui" line="2809"/>
        <location filename="mainwindow.ui" line="2926"/>
        <location filename="mainwindow.ui" line="2976"/>
        <source>Title:</source>
        <translation>  Имя:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1611"/>
        <source>Audio #9:</source>
        <translation>Аудио #9:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1712"/>
        <source>Audio #2:</source>
        <translation>Аудио #2:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1592"/>
        <source>Audio #4:</source>
        <translation>Аудио #4:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1432"/>
        <source>Audio #6:</source>
        <translation>Аудио #6:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1731"/>
        <source>Audio #1:</source>
        <translation>Аудио #1:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2098"/>
        <source>Audio #5:</source>
        <translation>Аудио #5:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1837"/>
        <source>Audio #8:</source>
        <translation>Аудио #8:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2150"/>
        <source>Subtitles</source>
        <translation>Субтитры</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2336"/>
        <source>Subtitle #7:</source>
        <translation>Субтит. #7:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2889"/>
        <source>Subtitle #3:</source>
        <translation>Субтит. #3:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2440"/>
        <source>Subtitle #9:</source>
        <translation>Субтит. #9:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2870"/>
        <source>Subtitle #2:</source>
        <translation>Субтит. #2:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2517"/>
        <source>Subtitle #4:</source>
        <translation>Субтит. #4:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2825"/>
        <source>Subtitle #6:</source>
        <translation>Субтит. #6:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2389"/>
        <source>Subtitle #1:</source>
        <translation>Субтит. #1:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2270"/>
        <source>Subtitle #5:</source>
        <translation>Субтит. #5:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2408"/>
        <source>Subtitle #8:</source>
        <translation>Субтит. #8:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="104"/>
        <source>Split</source>
        <translation>Разделить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="6507"/>
        <source>Next Frame  </source>
        <translation>След. кадр</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="6377"/>
        <location filename="mainwindow.ui" line="6544"/>
        <location filename="mainwindow.ui" line="6575"/>
        <source>00:00:00.000</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="6628"/>
        <source>Set end time</source>
        <translation>Уст. кон. вр.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="6402"/>
        <source>  Prev Frame</source>
        <translation>  Пред. кадр</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="6600"/>
        <source>Set start time</source>
        <translation>Уст. старт. вр.</translation>
    </message>
    <message>
        <source>Current time:</source>
        <translation type="vanished">Текущее время</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3336"/>
        <location filename="mainwindow.cpp" line="103"/>
        <location filename="mainwindow.cpp" line="3666"/>
        <source>Preview</source>
        <translation>Обзор</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3471"/>
        <location filename="mainwindow.ui" line="4988"/>
        <location filename="mainwindow.cpp" line="341"/>
        <source>Add files</source>
        <translation>Добавить файлы</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3607"/>
        <source>Output folder</source>
        <translation>Выход. папка</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4883"/>
        <source>Sort Z-A</source>
        <translation>Сорт. Z-A</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4055"/>
        <source>File name</source>
        <translation>Имя файла</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1236"/>
        <source>Clear all subtitle titles</source>
        <translation>Очистить все названия субтитров</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3043"/>
        <source>Clear all audio titles</source>
        <translation>Очистить все названия аудио</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3191"/>
        <source>Undo all titles</source>
        <translation>Вернуть все названия</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4063"/>
        <source>Format</source>
        <translation>Формат</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4071"/>
        <source>Resolution</source>
        <translation>Разрешение</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4079"/>
        <source>Duration</source>
        <translation>Длительн.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4087"/>
        <source>FPS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4095"/>
        <source>AR</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4103"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4111"/>
        <source>Bitrate</source>
        <translation>Битрейт</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4119"/>
        <source>Subsampling</source>
        <translation>Подвыборка</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4127"/>
        <source>Bit depth</source>
        <translation>Разрядность</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4135"/>
        <source>Color space</source>
        <translation>Цв. простр.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4143"/>
        <source>Color range</source>
        <translation>Цв. диап.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4151"/>
        <source>Color prim</source>
        <translation>Цв. исходный</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4159"/>
        <source>Color mtrx</source>
        <translation>Цв. матрица</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4167"/>
        <source>Transfer</source>
        <translation>Перемещение</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4175"/>
        <source>Max lum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4183"/>
        <source>Min lum</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4191"/>
        <source>Max CLL</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4199"/>
        <source>Max Fall</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4207"/>
        <source>Master display</source>
        <translation>Мастер дисп.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4215"/>
        <source>Path</source>
        <translation>Путь</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4223"/>
        <source>ID 21 - TECHNICAL INFO: Duration</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4231"/>
        <source>ID 22 - TECHNICAL INFO: Chroma coord</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4239"/>
        <source>ID 23 - TECHNICAL INFO: White coord</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4247"/>
        <source>ID 24 - TECHNICAL INFO: Stream size</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4255"/>
        <source>ID 25 - TECHNICAL INFO: Width</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4263"/>
        <source>ID 26 - TECHNICAL INFO: Height</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4271"/>
        <source>ID 27 - TECHNICAL INFO: VideoTitle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4279"/>
        <source>ID 28 - TECHNICAL INFO: Video Movie Name</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4287"/>
        <source>ID 29 - TECHNICAL INFO: Video Year</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4295"/>
        <source>ID 30 - TECHNICAL INFO: Video Author</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4303"/>
        <source>ID 31 - TECHNICAL INFO: Video Performer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4311"/>
        <source>ID 32 - TECHNICAL INFO: Video Description</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4319"/>
        <source>ID 33 - TECHNICAL INFO: Audio #1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4327"/>
        <source>ID 34 - TECHNICAL INFO: Audio #2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4335"/>
        <source>ID 35 - TECHNICAL INFO: Audio #3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4343"/>
        <source>ID 36 - TECHNICAL INFO: Audio #4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4351"/>
        <source>ID 37 - TECHNICAL INFO: Audio #5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4359"/>
        <source>ID 38 - TECHNICAL INFO: Audio #6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4367"/>
        <source>ID 39 - TECHNICAL INFO: Audio #7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4375"/>
        <source>ID 40 - TECHNICAL INFO: Audio #8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4383"/>
        <source>ID 41 - TECHNICAL INFO: Audio #9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4391"/>
        <source>ID 42 - TECHNICAL INFO: Audio Lang #1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4399"/>
        <source>ID 43 - TECHNICAL INFO: Audio Lang #2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4407"/>
        <source>ID 44 - TECHNICAL INFO: Audio Lang #3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4415"/>
        <source>ID 45 - TECHNICAL INFO: Audio Lang #4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4423"/>
        <source>ID 46 - TECHNICAL INFO: Audio Lang #5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4431"/>
        <source>ID 47 - TECHNICAL INFO: Audio Lang #6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4439"/>
        <source>ID 48 - TECHNICAL INFO: Audio Lang #7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4447"/>
        <source>ID 49 - TECHNICAL INFO: Audio Lang #8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4455"/>
        <source>ID 50 - TECHNICAL INFO: Audio Lang #9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4463"/>
        <source>ID 51 - TECHNICAL INFO: Audio Title #1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4471"/>
        <source>ID 52 - TECHNICAL INFO: Audio Title #2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4479"/>
        <source>ID 53 - TECHNICAL INFO: Audio Title #3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4487"/>
        <source>ID 54 - TECHNICAL INFO: Audio Title #4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4495"/>
        <source>ID 55 - TECHNICAL INFO: Audio Title #5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4503"/>
        <source>ID 56 - TECHNICAL INFO: Audio Title #6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4511"/>
        <source>ID 57 - TECHNICAL INFO: Audio Title #7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4519"/>
        <source>ID 58 - TECHNICAL INFO: Audio Title #8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4527"/>
        <source>ID 59 - TECHNICAL INFO: Audio Title #9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4535"/>
        <source>ID 60 - TECHNICAL INFO: Audio #1 Check State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4540"/>
        <source>ID 61 - TECHNICAL INFO: Audio #2 Check State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4545"/>
        <source>ID 62 - TECHNICAL INFO: Audio #3 Check State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4550"/>
        <source>ID 63 - TECHNICAL INFO: Audio #4 Check State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4555"/>
        <source>ID 64 - TECHNICAL INFO: Audio #5 Check State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4560"/>
        <source>ID 65 - TECHNICAL INFO: Audio #6 Check State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4565"/>
        <source>ID 66 - TECHNICAL INFO: Audio #7 Check State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4570"/>
        <source>ID 67 - TECHNICAL INFO: Audio #8 Check State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4575"/>
        <source>ID 68 - TECHNICAL INFO: Audio #9 Check State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4580"/>
        <source>ID 69 - TECHNICAL INFO: Subtitle #1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4585"/>
        <source>ID 70 - TECHNICAL INFO: Subtitle #2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4590"/>
        <source>ID 71 - TECHNICAL INFO: Subtitle #3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4595"/>
        <source>ID 72 - TECHNICAL INFO: Subtitle #4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4600"/>
        <source>ID 73 - TECHNICAL INFO: Subtitle #5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4605"/>
        <source>ID 74 - TECHNICAL INFO: Subtitle #6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4610"/>
        <source>ID 75 - TECHNICAL INFO: Subtitle #7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4615"/>
        <source>ID 76 - TECHNICAL INFO: Subtitle #8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4620"/>
        <source>ID 77 - TECHNICAL INFO: Subtitle #9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4625"/>
        <source>ID 78 - TECHNICAL INFO: Subtitle Lang #1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4630"/>
        <source>ID 79 - TECHNICAL INFO: Subtitle Lang #2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4635"/>
        <source>ID 80 - TECHNICAL INFO: Subtitle Lang #3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4640"/>
        <source>ID 81 - TECHNICAL INFO: Subtitle Lang #4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4645"/>
        <source>ID 82 - TECHNICAL INFO: Subtitle Lang #5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4650"/>
        <source>ID 83 - TECHNICAL INFO: Subtitle Lang #6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4655"/>
        <source>ID 84 - TECHNICAL INFO: Subtitle Lang #7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4660"/>
        <source>ID 85 - TECHNICAL INFO: Subtitle Lang #8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4665"/>
        <source>ID 86 - TECHNICAL INFO: Subtitle Lang #9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4670"/>
        <source>ID 87 - TECHNICAL INFO: Subtitle Title #1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4675"/>
        <source>ID 88 - TECHNICAL INFO: Subtitle Title #2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4680"/>
        <source>ID 89 - TECHNICAL INFO: Subtitle Title #3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4685"/>
        <source>ID 90 - TECHNICAL INFO: Subtitle Title #4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4690"/>
        <source>ID 91 - TECHNICAL INFO: Subtitle Title #5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4695"/>
        <source>ID 92 - TECHNICAL INFO: Subtitle Title #6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4700"/>
        <source>ID 93 - TECHNICAL INFO: Subtitle Title #7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4705"/>
        <source>ID 94 - TECHNICAL INFO: Subtitle Title #8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4710"/>
        <source>ID 95 - TECHNICAL INFO: Subtitle Title #9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4715"/>
        <source>ID 96 - TECHNICAL INFO: Subtitle #1 Check State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4720"/>
        <source>ID 97 - TECHNICAL INFO: Subtitle #2 Check State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4725"/>
        <source>ID 98 - TECHNICAL INFO: Subtitle #3 Check State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4730"/>
        <source>ID 99 - TECHNICAL INFO: Subtitle #4 Check State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4735"/>
        <source>ID 100 - TECHNICAL INFO: Subtitle #5 Check State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4740"/>
        <source>ID 101 - TECHNICAL INFO: Subtitle #6 Check State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4745"/>
        <source>ID 102 - TECHNICAL INFO: Subtitle #7 Check State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4750"/>
        <source>ID 103 - TECHNICAL INFO: Subtitle #8 Check State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4755"/>
        <source>ID 104 - TECHNICAL INFO: Subtitle #9 Check State</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4760"/>
        <source>ID 105 - TECHNICAL INFO: Start Time</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4765"/>
        <source>ID 106 - TECHNICAL INFO: End Time</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5112"/>
        <location filename="mainwindow.cpp" line="1617"/>
        <location filename="mainwindow.cpp" line="2888"/>
        <source>Encode</source>
        <translation>Кодировать</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5154"/>
        <source>Single Mode</source>
        <translation>Одиночный</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5159"/>
        <source>Batch Mode</source>
        <translation>Пакетный</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="3799"/>
        <source>Tasks</source>
        <translation>Задания</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4794"/>
        <location filename="mainwindow.cpp" line="342"/>
        <source>Remove from the list</source>
        <translation>Убрать из списка</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="4835"/>
        <location filename="mainwindow.cpp" line="349"/>
        <location filename="mainwindow.cpp" line="3206"/>
        <source>Stop</source>
        <translation>Стоп</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5068"/>
        <source>Sort A-Z</source>
        <translation>Сорт. A-Z</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="5195"/>
        <source>Scale</source>
        <translation>Масштаб</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="vanished">Очистить</translation>
    </message>
    <message>
        <source>Auto fill metadata</source>
        <translation type="vanished">Автозаполнение метаданных</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="6093"/>
        <source>Clear all</source>
        <translation>Очистить всё</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="6144"/>
        <source>Undo all</source>
        <translation>Вернуть всё</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="6707"/>
        <source>Reset labels</source>
        <translation>Сбросить метки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="6871"/>
        <location filename="mainwindow.ui" line="6874"/>
        <source>Preferences</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="6931"/>
        <location filename="mainwindow.cpp" line="284"/>
        <source>Hide</source>
        <translation>Скрыть</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="6984"/>
        <location filename="mainwindow.cpp" line="343"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="7018"/>
        <source>Add title for all files</source>
        <translation>Добавить заголовок для всех файлов</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="7021"/>
        <source>Title  (global)</source>
        <translation>Название (глобально)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="7040"/>
        <source>Expand</source>
        <translation>Развернуть</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="7078"/>
        <location filename="mainwindow.ui" line="7081"/>
        <source>Tools</source>
        <translation>Инструменты</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="7126"/>
        <location filename="mainwindow.ui" line="7129"/>
        <location filename="mainwindow.cpp" line="365"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="7218"/>
        <location filename="mainwindow.ui" line="7221"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="65"/>
        <source>No media</source>
        <translation>Нет медиа</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="72"/>
        <source>No audio</source>
        <translation>Нет аудио</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="79"/>
        <source>No subtitle</source>
        <translation>Нет субтитров</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="103"/>
        <source>Presets</source>
        <translation>Пресеты</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="103"/>
        <location filename="mainwindow.cpp" line="4732"/>
        <location filename="mainwindow.cpp" line="4733"/>
        <location filename="mainwindow.cpp" line="4734"/>
        <location filename="mainwindow.cpp" line="4735"/>
        <location filename="mainwindow.cpp" line="4736"/>
        <location filename="mainwindow.cpp" line="4737"/>
        <location filename="mainwindow.cpp" line="4738"/>
        <location filename="mainwindow.cpp" line="4739"/>
        <location filename="mainwindow.cpp" line="4740"/>
        <location filename="mainwindow.cpp" line="4741"/>
        <location filename="mainwindow.cpp" line="4754"/>
        <source>Source</source>
        <translation>Исходный</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="103"/>
        <source>Output</source>
        <translation>Выход</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="vanished">Опции</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="104"/>
        <source>Log</source>
        <translation>Лог</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="104"/>
        <source>Streams</source>
        <translation>Потоки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="161"/>
        <source>Quit program?</source>
        <translation>Выйти из программы?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="285"/>
        <source>Show</source>
        <translation>Показать</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="286"/>
        <source>Exit</source>
        <translation>Выйти</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="348"/>
        <source>Encode/Pause</source>
        <translation>Кодировать/Пауза</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="353"/>
        <source>Edit metadata</source>
        <translation>Редакт. метаданные</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="354"/>
        <source>Select audio streams</source>
        <translation>Выбрать аудио потоки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="355"/>
        <source>Select subtitles</source>
        <translation>Выбрать субтитры</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="356"/>
        <source>Split video</source>
        <translation>Разделить</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="366"/>
        <source>Donate</source>
        <translation>Донат</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="430"/>
        <location filename="mainwindow.cpp" line="462"/>
        <source>Add section</source>
        <translation>Добавить раздел</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="431"/>
        <source>Add preset</source>
        <translation>Добавить пресет</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="432"/>
        <source>Rename</source>
        <translation>Переименовать</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="463"/>
        <source>Add new preset</source>
        <translation>Добавить пресет</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1170"/>
        <source>You need to restart the program for the settings to take effect.</source>
        <translation>Вам необходимо перезапустить программу, чтобы настройки вступили в силу.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1350"/>
        <source> bit, </source>
        <translation> бит, </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1353"/>
        <source> kbps; </source>
        <translation> кбит/с; </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1365"/>
        <source>Audio #</source>
        <translation>Аудио #</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1380"/>
        <source>Undefined</source>
        <translation>Неопределенный</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2456"/>
        <source>Can&apos;t find color primaries %1 in source map.</source>
        <translation>Не удается найти основные цвета %1 на исходной карте.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2502"/>
        <source>Can&apos;t find color matrix %1 in source map.</source>
        <translation>Не удается найти цветовую матрицу %1 на исходной карте.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2552"/>
        <source>Can&apos;t find transfer characteristics %1 in source map.</source>
        <translation>Не удается найти характеристики переноса %1 на исходной карте.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2734"/>
        <source>Incorrect master display chroma coordinates source parameters!</source>
        <translation>Неверные параметры источника координат цветности главного дисплея!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2744"/>
        <source>Incorrect master display white point coordinates source parameters!</source>
        <translation>Неверное отображение основных параметров источника координат белой точки!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2880"/>
        <source>Muxing:</source>
        <translation>Микс:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2889"/>
        <source>The file does not contain FPS information!
Select the correct input file!</source>
        <translation>Файл не содержит информации о FPS!
Выберите правильный входной файл!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2893"/>
        <location filename="mainwindow.cpp" line="3188"/>
        <source>Encoding</source>
        <translation>Кодирование</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2968"/>
        <location filename="mainwindow.cpp" line="2973"/>
        <source>Encoding:</source>
        <translation>Кодирование:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2978"/>
        <source>1-st pass:</source>
        <translation>1-й проход:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2988"/>
        <source>An unknown error occurred!
 Possible FFMPEG not installed.
</source>
        <translation>Произошла неизвестная ошибка!
Возможно, FFMPEG не установлен.
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="3000"/>
        <source>Add data:</source>
        <translation>Данные:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="3009"/>
        <source>An unknown error occured!
 Possible mkvtoolnix not installed.
</source>
        <translation>Произошла неизвестная ошибка!
Возможно, mkvtoolnix не установлен.
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="3018"/>
        <source>Done!</source>
        <translation>Готово!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="3044"/>
        <location filename="mainwindow.cpp" line="3057"/>
        <source>Task completed!

 Elapsed time: </source>
        <translation>Задание выполнено!

Затраченное время: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="3168"/>
        <location filename="mainwindow.cpp" line="3238"/>
        <location filename="mainwindow.cpp" line="3316"/>
        <location filename="mainwindow.cpp" line="3337"/>
        <source>Pause</source>
        <translation>Пауза</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="3213"/>
        <source>The current encoding process has been canceled!
</source>
        <translation>Текущий процесс кодирования отменен!
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="3224"/>
        <source>Error!</source>
        <translation>Ошибка!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="3227"/>
        <source>An error occurred: </source>
        <translation>Обнаружена ошибка: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="3229"/>
        <source>Unexpected error occurred!</source>
        <translation>Произошла непредвиденная ошибка!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="3261"/>
        <source>Video Files: *.avi, *.m2ts, *.m4v, *.mkv, *.mov, *.mp4, *.mpeg, *.mpg, *.mxf, *.ts, *.webm (*.avi *.m2ts *.m4v *.mkv *.mov *.mp4 *.mpeg *.mpg *.mxf *.ts *.webm);;All files (*.*)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="3305"/>
        <source>Select input file first!</source>
        <translation>Сначала выберите исходный файл!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="3310"/>
        <source>Select preset first!</source>
        <translation>Сначала выберите пресет!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="3329"/>
        <source>Resume</source>
        <translation>Продолжить</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="3347"/>
        <source>Stop encoding?</source>
        <translation>Остановить кодирование?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="3374"/>
        <source>Unexpected error while trying to perform file name detection.</source>
        <translation>Непредвиденная ошибка при попытке выполнить обнаружение имени файла.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="3957"/>
        <source>Select output folder</source>
        <translation>Выбрать выходную папку</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4325"/>
        <location filename="mainwindow.cpp" line="4339"/>
        <location filename="mainwindow.cpp" line="4400"/>
        <location filename="mainwindow.cpp" line="4434"/>
        <source>Select preset first!
</source>
        <translation>Сначала выберите пресеты!
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4362"/>
        <source>Delete?</source>
        <translation>Удалить?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4390"/>
        <source>Delete presets first!
</source>
        <translation>Сначала удалите пресеты!
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4547"/>
        <source>Preset not selected</source>
        <translation>Пресет не выбран</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4612"/>
        <source>H.265/HEVC 4:2:0 10 bit</source>
        <translation>H.265/HEVC 4:2:0 10 бит</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4613"/>
        <source>H.265/HEVC 4:2:0 8 bit</source>
        <translation>H.265/HEVC 4:2:0 8 бит</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4614"/>
        <source>H.264/AVC 4:2:0 8 bit</source>
        <translation>H.264/AVC 4:2:0 8 бит</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4615"/>
        <source>Intel QSV H.264/AVC 4:2:0 8 bit</source>
        <translation>Intel QSV H.264/AVC 4:2:0 8 бит</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4616"/>
        <source>Intel QSV MPEG-2 4:2:0 8 bit</source>
        <translation>Intel QSV MPEG-2 4:2:0 8 бит</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4617"/>
        <source>NVENC H.265/HEVC 4:2:0 10 bit</source>
        <translation>NVENC H.265/HEVC 4:2:0 10 бит</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4618"/>
        <source>NVENC H.265/HEVC 4:2:0 8 bit</source>
        <translation>NVENC H.265/HEVC 4:2:0 8 бит</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4619"/>
        <source>NVENC H.264/AVC 4:2:0 8 bit</source>
        <translation>NVENC H.264/AVC 4:2:0 8 бит</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4620"/>
        <source>VP9 4:2:0 10 bit</source>
        <translation>VP9 4:2:0 10 бит</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4621"/>
        <source>VP9 4:2:0 8 bit</source>
        <translation>VP9 4:2:0 8 бит</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4622"/>
        <source>ProRes Proxy</source>
        <translation>ProRes Proxy</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4624"/>
        <source>ProRes Standard</source>
        <translation>ProRes Standard</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4634"/>
        <source>From source</source>
        <translation>Копия</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4652"/>
        <location filename="mainwindow.cpp" line="4653"/>
        <location filename="mainwindow.cpp" line="4654"/>
        <location filename="mainwindow.cpp" line="4655"/>
        <location filename="mainwindow.cpp" line="4656"/>
        <location filename="mainwindow.cpp" line="4657"/>
        <location filename="mainwindow.cpp" line="4658"/>
        <location filename="mainwindow.cpp" line="4659"/>
        <location filename="mainwindow.cpp" line="4660"/>
        <location filename="mainwindow.cpp" line="4661"/>
        <location filename="mainwindow.cpp" line="4662"/>
        <location filename="mainwindow.cpp" line="4664"/>
        <location filename="mainwindow.cpp" line="4705"/>
        <location filename="mainwindow.cpp" line="4706"/>
        <location filename="mainwindow.cpp" line="4712"/>
        <location filename="mainwindow.cpp" line="4713"/>
        <location filename="mainwindow.cpp" line="4714"/>
        <location filename="mainwindow.cpp" line="4715"/>
        <location filename="mainwindow.cpp" line="4716"/>
        <location filename="mainwindow.cpp" line="4717"/>
        <location filename="mainwindow.cpp" line="4718"/>
        <location filename="mainwindow.cpp" line="4719"/>
        <location filename="mainwindow.cpp" line="4720"/>
        <location filename="mainwindow.cpp" line="4721"/>
        <location filename="mainwindow.cpp" line="4722"/>
        <location filename="mainwindow.cpp" line="4723"/>
        <location filename="mainwindow.cpp" line="4724"/>
        <source>Auto</source>
        <translation>Авто</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4672"/>
        <location filename="mainwindow.cpp" line="4673"/>
        <location filename="mainwindow.cpp" line="4674"/>
        <location filename="mainwindow.cpp" line="4675"/>
        <location filename="mainwindow.cpp" line="4676"/>
        <location filename="mainwindow.cpp" line="4677"/>
        <location filename="mainwindow.cpp" line="4678"/>
        <location filename="mainwindow.cpp" line="4679"/>
        <location filename="mainwindow.cpp" line="4680"/>
        <location filename="mainwindow.cpp" line="4681"/>
        <location filename="mainwindow.cpp" line="4682"/>
        <location filename="mainwindow.cpp" line="4683"/>
        <location filename="mainwindow.cpp" line="4684"/>
        <location filename="mainwindow.cpp" line="4685"/>
        <location filename="mainwindow.cpp" line="4686"/>
        <location filename="mainwindow.cpp" line="4687"/>
        <location filename="mainwindow.cpp" line="4688"/>
        <location filename="mainwindow.cpp" line="4689"/>
        <location filename="mainwindow.cpp" line="4690"/>
        <location filename="mainwindow.cpp" line="4691"/>
        <location filename="mainwindow.cpp" line="4692"/>
        <location filename="mainwindow.cpp" line="4693"/>
        <location filename="mainwindow.cpp" line="4694"/>
        <source>None</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4672"/>
        <location filename="mainwindow.cpp" line="4673"/>
        <location filename="mainwindow.cpp" line="4674"/>
        <source>Ultrafast</source>
        <translation>Ultrafast</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4672"/>
        <location filename="mainwindow.cpp" line="4673"/>
        <location filename="mainwindow.cpp" line="4674"/>
        <source>Superfast</source>
        <translation>Superfast</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4672"/>
        <location filename="mainwindow.cpp" line="4673"/>
        <location filename="mainwindow.cpp" line="4674"/>
        <location filename="mainwindow.cpp" line="4675"/>
        <location filename="mainwindow.cpp" line="4676"/>
        <source>Veryfast</source>
        <translation>Veryfast</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4672"/>
        <location filename="mainwindow.cpp" line="4673"/>
        <location filename="mainwindow.cpp" line="4674"/>
        <location filename="mainwindow.cpp" line="4675"/>
        <location filename="mainwindow.cpp" line="4676"/>
        <source>Faster</source>
        <translation>Faster</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4672"/>
        <location filename="mainwindow.cpp" line="4673"/>
        <location filename="mainwindow.cpp" line="4674"/>
        <location filename="mainwindow.cpp" line="4675"/>
        <location filename="mainwindow.cpp" line="4676"/>
        <source>Fast</source>
        <translation>Fast</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4672"/>
        <location filename="mainwindow.cpp" line="4673"/>
        <location filename="mainwindow.cpp" line="4674"/>
        <location filename="mainwindow.cpp" line="4675"/>
        <location filename="mainwindow.cpp" line="4676"/>
        <source>Medium</source>
        <translation>Medium</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4672"/>
        <location filename="mainwindow.cpp" line="4673"/>
        <location filename="mainwindow.cpp" line="4674"/>
        <location filename="mainwindow.cpp" line="4675"/>
        <location filename="mainwindow.cpp" line="4676"/>
        <location filename="mainwindow.cpp" line="4677"/>
        <location filename="mainwindow.cpp" line="4678"/>
        <location filename="mainwindow.cpp" line="4679"/>
        <source>Slow</source>
        <translation>Slow</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4672"/>
        <location filename="mainwindow.cpp" line="4673"/>
        <location filename="mainwindow.cpp" line="4674"/>
        <location filename="mainwindow.cpp" line="4675"/>
        <location filename="mainwindow.cpp" line="4676"/>
        <source>Slower</source>
        <translation>Slower</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4672"/>
        <location filename="mainwindow.cpp" line="4673"/>
        <location filename="mainwindow.cpp" line="4674"/>
        <location filename="mainwindow.cpp" line="4675"/>
        <location filename="mainwindow.cpp" line="4676"/>
        <source>Veryslow</source>
        <translation>Veryslow</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4702"/>
        <location filename="mainwindow.cpp" line="4703"/>
        <location filename="mainwindow.cpp" line="4704"/>
        <location filename="mainwindow.cpp" line="4710"/>
        <location filename="mainwindow.cpp" line="4711"/>
        <source>1 Pass</source>
        <translation>1 Проход</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4702"/>
        <location filename="mainwindow.cpp" line="4703"/>
        <location filename="mainwindow.cpp" line="4704"/>
        <location filename="mainwindow.cpp" line="4707"/>
        <location filename="mainwindow.cpp" line="4708"/>
        <location filename="mainwindow.cpp" line="4709"/>
        <location filename="mainwindow.cpp" line="4710"/>
        <location filename="mainwindow.cpp" line="4711"/>
        <source>2 Pass</source>
        <translation>2 Прохода</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="4880"/>
        <source>Task</source>
        <translation>Задача</translation>
    </message>
</context>
</TS>
